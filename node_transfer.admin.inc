<?php

/**
 * @file
 * Configuration panel for the node_transfer module.
 */

/**
 * FAPI defintion for the node_transfer module admin page.
 */
function node_transfer_admin($form, $form_state) {

  $type_options = array();

  foreach (node_type_get_types() as $a_type) {
    $type_options[$a_type->type] = $a_type->name;
  }

  return array(
    'node_transfer_content_types_container' => array(
      '#type' => 'fieldset',
      '#collapsable' => FALSE,
      '#title' => t('Node Transfer Content Types'),
      '#description' => t('Configure which content types are transferable using the node transfer module.'),
      'node_transfer_content_types' => array(
        '#type' => 'checkboxes',
        '#title' => t('Enabled Types'),
        '#options' => $type_options,
        '#default_value' => variable_get('node_transfer_node_types', array()),
      ),
    ),
    'node_transfer_container' => array(
      '#type' => 'fieldset',
      '#collapsable' => FALSE,
      '#title' => t('Node Transfer Messages'),
      '#description' => t('Use this form to configure the emails sent out during the node transferring process.'),
      'node_transfer_receipt_message' => array(
        '#type' => 'textarea',
        '#rows' => 10,
        '#default_value' => variable_get('node_transfer_receipt_message'),
        '#title' => t('Node Transfer Reciept Message'),
        '#description' => t('This message is sent to users when they transfer a node.  It can include the below mentioned replacement patterns.  <strong>Note:</strong> that it should also include the text <strong>[node-transfer-token]</strong> wherever the provided secrete token should appear in the message.'),
      ),
      'node_transfer_claim_message' => array(
        '#type' => 'textarea',
        '#rows' => 10,
        '#default_value' => variable_get('node_transfer_claim_message'),
        '#title' => t('Node Transfer Complete Message'),
        '#description' => t('This message that will be sent to users who transferred content, once that content has successfully been claimed by its new owner.'),
      ),
      'tokens' => array(
        '#theme' => 'token_tree',
        '#token_types' => array('node', 'user'),
        '#global_types' => TRUE,
        '#click_insert' => TRUE,
      ),
    ),
    'actions' => array(
      '#type' => 'actions',
      'submit' => array(
        '#type' => 'submit',
        '#value' => t('Save Settings'),
      ),
      'cancel' => array(
        '#type' => 'button',
        '#value' => t('Cancel'),
      ),
    ),
  );
}

/**
 * FAPI form submit callback for the node transfer admin panel.
 */
function node_transfer_admin_submit($form, &$form_state) {

  $values = $form_state['values'];

  variable_set('node_transfer_receipt_message', $values['node_transfer_receipt_message']);
  variable_set('node_transfer_claim_message', $values['node_transfer_claim_message']);
  variable_set('node_transfer_node_types', $values['node_transfer_content_types']);

  drupal_set_message(t('Settings successfully updated.'), 'status');
}
