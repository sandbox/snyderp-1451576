<?php

/**
 * @file
 * Contains views integration hooks, to describe node transfer history
 * information to the views system.
 */

/**
 * Implements hook_views_data().
 */
function node_transfer_views_data() {

  return array(
    'node_transfer_records' => array(
      'table' => array(
        'group' => t('Node Transfer'),
        'base' => array(
          'field' => 'ntr_id',
          'title' => t('Node Transfer Records'),
          'help' => t('Contains information about the transfer history of nodes.'),
        ),
        'join' => array(
          'node' => array(
            'left_field' => 'nid',
            'field' => 'nid',
          ),
        ),
      ),
      'ntr_id' => array(
        'title' => t('Node Transfer Record ID'),
        'help' => t('The unique identifier of a node transfer record'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_numeric',
        ),
      ),
      'nid' => array(
        'title' => t('Node ID'),
        'help' => t('The unique ID of a node that was tranferred'),
        'relationship' => array(
          'base' => 'node',
          'field' => 'nid',
          'handler' => 'views_handler_relationship',
          'label' => t('Node'),
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_node_nid',
        ),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_numeric',
        ),
      ),
      'from_uid' => array(
        'title' => t('Previous Owner'),
        'help' => t('The previous owner of a node, who transferred it to another user'),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_user_current',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_user_uid',
        ),
        'relationship' => array(
          'base' => 'users',
          'base field' => 'uid',
          'handler' => 'views_handler_relationship',
          'label' => t('Node Previous Owner'),
        ),
      ),
      'to_uid' => array(
        'title' => t('New Owner'),
        'help' => t('The new owner of a node, who claimed it from its previous owner'),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_user_current',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_user_uid',
        ),
        'relationship' => array(
          'base' => 'users',
          'relationship table' => 'users',
          'field' => 'to_uid',
          'handler' => 'views_handler_relationship',
          'label' => t('Node Recipient'),
        ),
      ),
      'transferred_on' => array(
        'title' => t('Transfer Date'),
        'help' => t('The date the node was transferred from its previous owner'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
      ),
      'claimed_on' => array(
        'title' => t('Claim Date'),
        'help' => t('The date the node was claimed by its new owner'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
      ),
      'token' => array(
        'title' => t('Token / Secret'),
        'help' => t('The secret token that a user needs to claim the node as herown'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
    ),
  );
}
