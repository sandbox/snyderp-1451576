Node Transfer
===

About
---

This module allows users to transfer content between themselves, without needing
to know each other's user name or needing to invove the site administrator.

This module uses a token / secret passing system, so a user looking to transfer
a node to another user is given a secret token, which they're then expected to
provide to the user that will receive the node.  This second user can then
use the provided token to claim the node as theirs.


Configuration
---

The node_transfer module's configuration panel is at admin/config/node-transfer
and allows administrators to configure which node types are transferable,
and the body of the email messages set to the owner of the transferred nodes. 


Usage
---

Once the node_transfer module is installed, a "transfer" link will appear in the
links section of nodes of any type set to be transferrable when a user is
viewing their own content.  Clicking on this link will take the user to a
confirmation page, asking if they're sure they want to transfer the node.  

If the user choose to still transfer the instrument by clicking a confirmation
button, ownership of the node is immediately transfered to the anonymous user.
An email is also sent to the transferring node owner, which contains a secret
token.  The transferring user is then expected to give this token to the site
user they want to turn ownership over to.

The link section of the node will now contain a "claim" link.  When clicked,
users will be taken to a new form, asking them for the secret token to claim
the node.  If entered correctly, they'll be the new owner of the node.  An email
will also be sent out to the node's previous owner.

 - snyderp <snyderp@gmail.com>