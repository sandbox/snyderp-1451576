<?php

/**
 * @file
 * Form definitions, intended to be used as menu hook page callbacks,
 * to allow users to transfer and claim nodes.
 */

/* ================================ */
/* ! Node Transfer FAPI Defintions  */
/* ================================ */

/**
 * FAPI defintion to confirm that a user really wants to give up ownership
 * of a node, so that another user can claim and then own it.
 */
function node_transfer_node_transfer_form($form, $form_state, $node) {

  return array(
    'transferred_node_nid' => array(
      '#type' => 'value',
      '#value' => $node->nid,
    ),
    'node_transfer_description' => array(
      '#markup' => '<div class="description">' . t('Do you want to transfer your instrument to another user? If you do, this instrument will be unpublished and you will get a transfer code via e-mail. You can then give this code to another user, who can can use it to claim the instrument as theirs.') . '</div>',
    ),
    'node_transfer_description_cont' => array(
      '#markup' => '<div class="description">' . t('Once you transfer this piece of gear, you will get an e-mail with instructions on how to complete the transfer process to another user.  You should also get a transfer confirmation once the instrument has been claimed.') . '</div>',
    ),
    'node_transfer_container' => array(
      '#type' => 'fieldset',
      '#collapsable' => FALSE,
      '#title' => t('Transfer Confirmation'),
      '#description' => t('Are you sure you want to transfer this gear to another user? The instrument will be inaccessible until you or someone you give the code to claim it.'),
      'actions' => array(
        '#type' => 'actions',
        'submit' => array(
          '#type' => 'submit',
          '#value' => t('Transfer this content'),
        ),
        'cancel' => array(
          '#markup' => l(t('Cancel'), 'node/' . $node->nid),
        ),
      ),
    ),
  );
}

/**
 * FAPI definition to actually process the node transfer process.  First
 * records the node transfer process, then edits the node so that it
 * belongs to the "Transfer" user.
 */
function node_transfer_node_transfer_form_submit($form, &$form_state) {

  module_load_include('inc', 'node_transfer', 'node_transfer.api');

  global $user;

  $values = $form_state['values'];
  $node = node_load($values['transferred_node_nid']);

  $token = node_transfer_api_record_transfer($node);

  if (!$token) {

    drupal_set_message(t('There was an error attempting to transfer %node-title.  Please try again later.', array('%node->title' => $node->title)), 'error');
  }
  else {

    $node->uid = 0;
    node_save($node);
    drupal_set_message(t('%node-title is now waiting to be claimed.  The claim code is: %token.  Please give this code to the new owner of this instrument in order to claim it.  A message with this information was also sent to %email.', array('%node-title' => $node->title, '%token' => $token, '%email' => $user->mail)), 'status');

    $mail_params = array(
      'token' => $token,
      'transferred_node' => $node,
      'from_user' => $user,
    );

    $modules = module_implements('node_transfer_began');
    foreach ($modules as $module) {
      module_invoke($module, 'node_transfer_began', $node);
    }

    drupal_mail('node_transfer', 'receipt_message', $user->mail, user_preferred_language($user), $mail_params);
  }

  $form_state['redirect'] = 'node/' . $node->nid;
}

/**
 * FAPI definition for users claiming a node that has already been transfered.
 */
function node_transfer_node_claim_form($form, $form_state, $node) {

  return array(
    'claimed_node_nid' => array(
      '#type' => 'value',
      '#value' => $node->nid,
    ),
    'node_claim_description' => array(
      '#markup' => '<div class="description">' . t('This page allows you to claim a piece of gear that has been transferred to you from another user.  To claim this content, enter the claim code you received below.') . '</div>',
    ),
    'node_claim_container' => array(
      '#type' => 'fieldset',
      '#title' => t('Claim Confirmation'),
      '#collapsable' => FALSE,
      '#description' => t('To claim %node-title, enter the claim code you were given below and click "Claim"', array('%node-title' => $node->title)),
      'claim_token' => array(
        '#type' => 'textfield',
        '#title' => t('Secret Token'),
      ),
      'actions' => array(
        '#type' => 'actions',
        'submit' => array(
          '#type' => 'submit',
          '#value' => t('Claim content'),
        ),
        'cancel' => array(
          '#markup' => l(t('Cancel'), 'node/' . $node->nid),
        ),
      ),
    ),
  );
}

/**
 * FAPI submission definition.  Checks one last time to make sure that the node
 * is still up for being transferred, and if so, records that the transfer
 * is complete, saves the node to is new owner, and emails the node's previous
 * owner that the claim is complete.
 */
function node_transfer_node_claim_form_submit($form, &$form_state) {

  module_load_include('inc', 'node_transfer', 'node_transfer.api');

  global $user;

  $values = $form_state['values'];
  $node = node_load($values['claimed_node_nid']);

  if (!$node OR !node_transfer_api_is_node_transfered($node)) {

    drupal_set_message(t('We were unable to find the transferred instrument.  Please try again later.'), 'error');
    $form_state['redirect'] = 'node/' . $values['claimed_node_nid'];
  }
  else {

    $token = $values['claim_token'];

    if (!node_transfer_api_record_claim($node->nid, $token, $user->uid)) {

      drupal_set_message(t('The claim code %token is invalid.  Please double check your records and try again.', array('%token' => $token)), 'error');
      $form_state['redirect'] = 'node/' . $values['claimed_node_nid'];
    }
    else {

      $node->uid = $user->uid;
      node_save($node);

      $previous_user_uid = node_transfer_api_previous_owner($node->nid, $user->uid);
      $previous_user = user_load($previous_user_uid);

      drupal_set_message(t('Congratulations! You have successfully claimed %node-title!  A message has been sent to the previous owner, letting them know that the transfer process has been completed.', array('%node-title' => $node->title)), 'status');

      $modules = module_implements('node_transfer_complete');
      foreach ($modules as $module) {
        module_invoke($module, 'node_transfer_complete', $node);
      }

      $mail_params = array(
        'transferred_node' => $node,
        'from_user' => $previous_user,
      );

      drupal_mail('node_transfer', 'claim_complete_message', $user->mail, user_preferred_language($user), $mail_params);

      $form_state['redirect'] = 'node/' . $node->nid;
    }
  }
}