<?php

/**
 * @file
 * This file contains all database functions / interactions between the
 * node_transfer module and its database storage.
 */

/**
 * Records that a node has been put up for transfer.  Node that this function
 * does not make the node unpublished, and the caller should handle that (if
 * thats what they want to do…)
 *
 * @param object $node
 *   The populated node object to record for being transfered
 *
 * @return string|bool
 *   The token for the the transfer on success, that another user can use to
 *   claim the node.  Otherwise, returns FALSE on invalid input
 */
function node_transfer_api_record_transfer($node) {

  if (empty($node) OR !is_numeric($node->nid)) {

    return FALSE;
  }
  else {

    $token = sha1(rand() . $node->nid . $node->uid . REQUEST_TIME);

    $rs = db_insert('node_transfer_records')
      ->fields(array(
        'nid' => $node->nid,
        'from_uid' => $node->uid,
        'transferred_on' => REQUEST_TIME,
        'token' => $token,
      ))
      ->execute();

    return is_numeric($rs) ? $token : FALSE;
  }
}

/**
 * Records that a user is claiming a node, that was previously put up for
 * transfer.  Note that this function does not actually change the node
 * record at all.  Callers of this function should check for a TRUE result
 * and, if so, update the node accordingly.
 *
 * @param int $nid
 *   The unique id of the node that is being claimed
 * @param string $token
 *   The provided identifier for the transfer, needed to calim the node
 * @param int $uid
 *   The unique id of the user claiming the node
 *
 * @return boolean
 *   TRUE if the transfer was successful and the given user now owns the node.
 *   Otherwise, returns FALSE.
 */
function node_transfer_api_record_claim($nid, $token, $uid) {

  if (!is_numeric($nid) OR empty($token) OR !is_numeric($uid)) {

    return FALSE;
  }
  else {

    $rs = db_select('node_transfer_records', 'ntr')
      ->fields('ntr', array('ntr_id'))
      ->condition('nid', $nid)
      ->condition('token', $token)
      ->range(0, 1)
      ->execute();

    if ($rs->rowCount() < 1) {

      return FALSE;
    }
    else {

      $rs = db_update('node_transfer_records')
        ->fields(array(
          'claimed_on' => REQUEST_TIME,
          'to_uid' => $uid,
        ))
        ->condition('nid', $nid)
        ->condition('token', $token)
        ->execute();

      return !! $rs;
    }
  }
}

/**
 * Returns the UID of the user that owned the node before the provided user.
 *
 * @param int $nid
 *   The unique id of the node that is being claimed
 * @param int $current_owner_uid
 *   The unique id of a user that claimed the node at one point
 *
 * @return int|boolean
 *   Either the uid of the user that transferred the node to the provided
 *   user, or FALSE if no match exists.
 */
function node_transfer_api_previous_owner($nid, $current_owner_uid) {

  if (empty($nid) OR !is_numeric($nid) OR empty($current_owner_uid) OR !is_numeric($current_owner_uid)) {

    return FALSE;
  }
  else {

    $rs = db_select('node_transfer_records', 'ntr')
      ->fields('ntr', array('from_uid'))
      ->condition('nid', $nid)
      ->condition('to_uid', $current_owner_uid)
      ->range(0, 1)
      ->execute();

    return $rs->rowCount() ? $rs->fetchObject()->from_uid : FALSE;
  }
}

/**
 * Checks to see if a given node has been transfered but not claimed.
 *
 * @param object $node
 *   A populated node object
 *
 * @return boolean
 *   TRUE if the given node has been transfered by its owner but hasn't been
 *   claimed by a new one.  Otherwise, FALSE.
 */
function node_transfer_api_is_node_transfered($node) {

  if (empty($node) OR !is_numeric($node->nid)) {

    return FALSE;
  }
  else {

    $rs = db_select('node_transfer_records', 'ntr')
      ->fields('ntr', array('ntr_id'))
      ->condition('nid', $node->nid)
      ->condition('claimed_on', '0' )
      ->range(0, 1)
      ->execute();

    return !! $rs->rowCount();
  }
}
